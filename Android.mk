LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

#LOCAL_VENDOR_MODULE := true
LOCAL_SRC_FILES += spitest.c
LOCAL_MODULE := spitest
 
LOCAL_LDFLAGS += -L$(LOCAL_PATH)
LOCAL_LDLIBS := -llog

include $(BUILD_EXECUTABLE)
